#pragma once

#include "Piece.h"


class Pawn : public Piece
{
public:
	/*class methods*/
	Pawn(char type, char color, std::string location);
	~Pawn();
	virtual void move(std::string location);

	/*member*/
	bool _ifPawnMovedAlready = false;
};