#pragma once

#include "Piece.h"


class Rook : public Piece
{
public:
	Rook(char type, char color, std::string location);
	~Rook();
	virtual void move(std::string location);
};