#pragma once
#include <string>
#include <iostream>

class Piece
{
public:
	/*class methods*/
	Piece(char type, char color, std::string location);
	~Piece();
	std::string getLocation();
	char getType();
	char getColor();
	void setterLocation(std::string location);
	virtual void move(std::string location) = 0;

protected:
	/*members*/
	char _type;
	char _color;
	std::string _location;

};

