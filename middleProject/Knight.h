#pragma once
#include "Piece.h"

class Knight : public Piece
{
public:
	Knight(char type, char color, std::string location);
	~Knight();
	virtual void move(std::string location);
};