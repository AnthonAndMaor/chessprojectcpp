#include "Pipe.h"
#include "GameManager.h"
#include <iostream>
#include <thread>

#define LOCATION_LEN 2
#define ORIGIN_START 0
#define TARGET_START 2
#define EMPTY_SQUARE nullptr

using namespace std;

void main()
{
	/*variables definition*/
	char startingBoard[] = "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR0";
	GameManager mainManager;
	mainManager.initPiecesVector();
	std::string moveCode = "";
	Piece* thePieceTheUserWantToMove = nullptr;
	char msgToGraphics[1024];
	char answerToGraphics[1024] = {0};
	int countTurns = 0;


	srand(time_t(NULL));
	Pipe p;
	bool isConnect = p.connect();

	string ans;
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else
		{
			p.close();
			return;
		}
	}

	// msgToGraphics should contain the board string accord the protocol
	// YOUR CODE

	strcpy_s(msgToGraphics, startingBoard);

	p.sendMessageToGraphics(msgToGraphics);   // send the board string

	// get message from graphics
	string msgFromGraphics = p.getMessageFromGraphics();
	countTurns++;

	while (msgFromGraphics != "quit")
	{
		// should handle the string the sent from graphics
		// according the protocol. Ex: e2e4           (move e2 to e4)

		/*checking whose turn is that*/
		if (countTurns % 2 == 0) 
		{
			mainManager.setTurn('b'); /*black's turn*/
		}
		else
		{
			mainManager.setTurn('w'); /*white's turn*/
		}

		/*checking which code */
		moveCode = mainManager.checkMove(msgFromGraphics);

		answerToGraphics[0] = moveCode[0];

		/*checking if we need to change the piece location*/
		if (!(moveCode.compare("1")) || !(moveCode.compare("0")))
		{
			thePieceTheUserWantToMove = mainManager.getPieceInLocation(msgFromGraphics.substr(ORIGIN_START, LOCATION_LEN));
			thePieceTheUserWantToMove->move(msgFromGraphics.substr(TARGET_START, LOCATION_LEN));
		}

		// send move code
		strcpy_s(msgToGraphics, answerToGraphics); // msgToGraphics should contain the result of the operation

		// return result to graphics		
		p.sendMessageToGraphics(msgToGraphics);

		// get message from graphics
		msgFromGraphics = p.getMessageFromGraphics();

		/*checking if we need to change the turn*/
		if (!moveCode.compare("1") || !moveCode.compare("0"))
		{
			countTurns++;
		}
	}

	p.close();
}