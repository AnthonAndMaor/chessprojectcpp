#pragma once

#include "Piece.h"


class Queen : public Piece
{
public:
	Queen(char type, char color, std::string location);
	~Queen();
	virtual void move(std::string location);
};