#pragma once
#include "Piece.h"
class Bishop : public Piece
{
public:
	Bishop(char type, char color, std::string location);
	~Bishop();
	virtual void move(std::string location);
};