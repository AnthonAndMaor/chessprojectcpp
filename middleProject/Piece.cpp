#include "Piece.h"

Piece::Piece(char type,char color, std::string location)
{
	this->_type = type;
	this->_color = color;
	this->_location = location;
}

Piece::~Piece()
{
}

std::string Piece::getLocation()
{
	return _location;
}

char Piece::getType()
{
	return _type;
}

char Piece::getColor()
{
	return _color;
}

void Piece::setterLocation(std::string location)
{
	_location = location;
}