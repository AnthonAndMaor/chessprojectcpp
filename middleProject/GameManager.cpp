
#include "GameManager.h"
#include "Piece.h"
#include "King.h"
#include "Rook.h"
#include "Queen.h"
#include "Bishop.h"
#include "Pawn.h"
#include "Knight.h"


using namespace std;


#define LOCATION_LEN 2
#define ORIGIN_START 0
#define TARGET_START 2

#define BLACK 'b'
#define WHITE 'w'

/*Set the turn*/
void GameManager::setTurn(char turn)
{
	this->_turn = turn;
}

/*The function initialize the vector*/
void GameManager::initPiecesVector()
{
	/*putting all the pieces in the vector*/

	/*black pieces*/
	this->_pieces.push_back(new Rook('r', BLACK, "a8"));
	this->_pieces.push_back(new Knight('n', BLACK, "b8"));
	this->_pieces.push_back(new Bishop('b', BLACK, "c8"));
	this->_pieces.push_back(new King('k', BLACK, "d8"));
	this->_pieces.push_back(new Queen('q', BLACK, "e8"));
	this->_pieces.push_back(new Bishop('b', BLACK, "f8"));
	this->_pieces.push_back(new Knight('n', BLACK, "g8"));
	this->_pieces.push_back(new Rook('r', BLACK, "h8"));

	this->_pieces.push_back(new Pawn('p', BLACK, "a7"));
	this->_pieces.push_back(new Pawn('p', BLACK, "b7"));
	this->_pieces.push_back(new Pawn('p', BLACK, "c7"));
	this->_pieces.push_back(new Pawn('p', BLACK, "d7"));
	this->_pieces.push_back(new Pawn('p', BLACK, "e7"));
	this->_pieces.push_back(new Pawn('p', BLACK, "f7"));
	this->_pieces.push_back(new Pawn('p', BLACK, "g7"));
	this->_pieces.push_back(new Pawn('p', BLACK, "h7"));

	
	/*white pieces*/
	this->_pieces.push_back(new Rook('R', WHITE, "a1"));
	this->_pieces.push_back(new Knight('N', WHITE, "b1"));
	this->_pieces.push_back(new Bishop('B', WHITE, "c1"));
	this->_pieces.push_back(new King('K', WHITE, "d1"));
	this->_pieces.push_back(new Queen('Q', WHITE, "e1"));
	this->_pieces.push_back(new Bishop('B', WHITE, "f1"));
	this->_pieces.push_back(new Knight('N', WHITE, "g1"));
	this->_pieces.push_back(new Rook('R', WHITE, "h1"));

	this->_pieces.push_back(new Pawn('P', WHITE, "a2"));
	this->_pieces.push_back(new Pawn('P', WHITE, "b2"));
	this->_pieces.push_back(new Pawn('P', WHITE, "c2"));
	this->_pieces.push_back(new Pawn('P', WHITE, "d2"));
	this->_pieces.push_back(new Pawn('P', WHITE, "e2"));
	this->_pieces.push_back(new Pawn('P', WHITE, "f2"));
	this->_pieces.push_back(new Pawn('P', WHITE, "g2"));
	this->_pieces.push_back(new Pawn('P', WHITE, "h2"));
}

/*The function gets piece in specific location*/
Piece* GameManager::getPieceInLocation(std::string location)
{
	/*running over all the pieces*/
	for (int i = 0; i < this->_pieces.size(); i++)
	{
		if (!(location.compare(this->_pieces[i]->getLocation()))) /*checking if the locations match*/
		{
			return this->_pieces[i]; /*returning the piece in the location*/
		}
	}
	return nullptr; /*no piece in this location*/
}


/*The function manages the checks and returning the right code for the fronted*/
std::string GameManager::checkMove(std::string enteredMove)
{
	/*variables definition*/
	int i = 0;
	string originLocation = enteredMove.substr(ORIGIN_START, LOCATION_LEN);
	string targetLocation = enteredMove.substr(TARGET_START, LOCATION_LEN);
	short indexOfOriginPieceInVector = 0;
	short indexOfTargetPieceInVector = 0;
	bool thereIsPieceInOriginLocation = false;
	bool thereIsPieceInTargetLocation = false;
	Piece* piece = nullptr;
	char pieceType = 'a';


	/*Check if the origin and target squares are valid.*/
	if (!((originLocation[0] >= 'a' && originLocation[0] <= 'h' && originLocation[1] >= '1' && originLocation[1] <= '8') &&
		(targetLocation[0] >= 'a' && targetLocation[0] <= 'h' && targetLocation[1] >= '1' && targetLocation[1] <= '8')))
	{
		return "5";/*Invalid indexes.*/
	}

	if (!originLocation.compare(targetLocation))
	{
		return "7"; /*the origin and target locations are the same.*/
	}

	/*Check what peices are located in the 2 squares (if there are some).*/
	for (int i = 0; i < this->_pieces.size(); i++)
	{
		if (!(this->_pieces[i]->getLocation().compare(originLocation)))/*Check if there is a peice in that location*/
		{
			/*piece found*/
			indexOfOriginPieceInVector = i;
			thereIsPieceInOriginLocation = true;
		}
		if (!(this->_pieces[i]->getLocation().compare(targetLocation)))/*check if there is a piece in the taget location*/
		{
			/*piece found*/
			indexOfTargetPieceInVector = i;
			thereIsPieceInTargetLocation = true;
		}
	}

	/*checking if there is no piece in the origin location or its not your turn*/
	if (!thereIsPieceInOriginLocation || getPieceInLocation(originLocation)->getColor() != this->_turn)
	{
		return "2"; /*There is no piece in the origin location or an enemy piece*/
	}

	/*Check if the piece at the target location is friendly or enemy.*/
	if (thereIsPieceInTargetLocation)
	{
		/*If the piece at the target is a friendly piece.*/
		if (isupper((this->_pieces[indexOfTargetPieceInVector]->getType())) && (this->_turn == WHITE) ||
			(islower(this->_pieces[indexOfTargetPieceInVector]->getType()) && (this->_turn == BLACK)))
		{
			return "3"; /*The target piece is friednly and you cant move to that square.*/
		}
	}
	
	/*finding the piece the user want to move*/
	for (int i = 0; i < this->_pieces.size(); i++)
	{
		if (!(originLocation.compare(this->_pieces[i]->getLocation())))
		{
			piece = _pieces[i]; /*saving the piece*/
			pieceType = this->_pieces[i]->getType(); /*saving the type of the piece*/
			break;
		}
	}

	if (!(ifPieceMoveRight(pieceType, originLocation, targetLocation)))
	{
		return "6"; /*the piece doesn't move as it should*/
	}
	
	if (ifKingIsUnderCheck(originLocation, targetLocation, targetLocation))
	{
		return "4"; /*cant move piece because it will cause check*/
	}

	piece->setterLocation(originLocation); /*returning the origin location to the piece*/

	/*Check if the piece at the target location is friendly or enemy - if its an enemy piece we can kill it*/
	if (thereIsPieceInTargetLocation)
	{
		if(!(isupper((this->_pieces[indexOfTargetPieceInVector]->getType())) && (this->_turn == WHITE) ||
			(islower(this->_pieces[indexOfTargetPieceInVector]->getType()) && (this->_turn == BLACK))))
		{
			this->_pieces.erase(this->_pieces.begin() + indexOfTargetPieceInVector); /*the piece is an enemy and we erasing it from the vector*/
		}
	}

	/*checking 1 code*/
	if (checkIfAfterThisMoveTheKingWillBeChecked(originLocation, targetLocation))
	{
		return "1"; /*there will be a check after this move*/
	}
	piece->setterLocation(originLocation); /*returning the origin location to the piece*/

	return "0"; /*regular move - all the checks have been made*/
}

/*The function checks if our last move cause check*/
bool GameManager::checkIfAfterThisMoveTheKingWillBeChecked(std::string origin, std::string target)
{
	/*variables definition*/
	short indexOfKing = 0;
	short movedPieceIndex = 0;
	Piece* removePiece = nullptr;
	string kingLocation = "";
	bool removed = false;
	string newLocation = target;

	/*Save the origin location because we want to move the piece out of the way.*/
	string tempLocation = origin;

	/*Find the index of the needed king.*/
	for (int i = 0; i < this->_pieces.size(); i++)
	{
		if (this->_pieces[i]->getType() == 'K' && this->_turn != WHITE ||
			this->_pieces[i]->getType() == 'k' && this->_turn != BLACK)
		{
			indexOfKing = i;
			kingLocation = this->_pieces[i]->getLocation(); /*saving the king's location*/
			break;
		}
	}


	/*Moving the piece out of the way.*/
	for (int i = 0; i < this->_pieces.size(); i++)
	{
		if (!(this->_pieces[i]->getLocation().compare(origin)))
		{
			/*moving the piece to other location*/
			movedPieceIndex = i;

			/*there is a piece in the new location so we need to move it away*/
			if (getPieceInLocation(newLocation) != nullptr)
			{
				/*removing the piece*/
				removePiece = getPieceInLocation(newLocation);
				getPieceInLocation(newLocation)->setterLocation("xx");
				removed = true;
			}
			this->_pieces[i]->setterLocation(newLocation); /*setting the new location*/
			break;
		}
	}


	/*checking if check will occure*/
	for (int i = 0; i < this->_pieces.size(); i++)
	{
		if (this->_pieces[i]->getColor() != this->_pieces[indexOfKing]->getColor()) /*checking if it is an enemy piece*/
		{
			/*checking if the enemy piece can get to the king location*/
			if (ifPieceMoveRight(this->_pieces[i]->getType(), this->_pieces[i]->getLocation(), kingLocation))
			{
				this->_pieces[movedPieceIndex]->setterLocation(tempLocation);  /*returning the old location*/
				if (removed)
				{
					removePiece->setterLocation(newLocation); /*returning the old location*/
				}
				return true; /*there will be a check*/
			}
		}
	}

	return false; /*there won't be a check*/
}

/*checking if the move is valid*/
bool GameManager::ifPieceMoveRight(char type, std::string origin, std::string target)
{

	if (type == 'r' || type == 'R') /*the piece is a rook*/
	{
		return checkIfRookCanMoveRight(origin, target); /*checking the move*/
	}
	else if (type == 'n' || type == 'N') /*the piece is a knight*/
	{
		return checkIfKnightCanMoveRight(origin, target); /*checking the move*/
	}
	else if (type == 'b' || type == 'B') /*the piece is a bishop*/
	{
		return checkIfBishopCanMoveRight(origin, target); /*checking the move*/
	}
	else if (type == 'q' || type == 'Q') /*the piece is a queen*/
	{
		return checkIfQueenCanMoveRight(origin, target); /*checking the move*/
	}
	else if (type == 'k' || type == 'K') /*the piece is a king*/
	{
		return checkIfKingCanMoveRight(origin, target); /*checking the move*/
	}
	else /*the piece is a pawn*/
	{
		return checkIfPawnCanMoveRight(origin, target, getPawnInLocation(origin)->getColor()); /*checking the move*/
	}
	

}

/*checking if the rook moves right*/
bool GameManager::checkIfRookCanMoveRight(std::string origin, std::string target)
{
	/*variables definition*/
	int originNum = (int)(origin[1] - '0'), targetNum = (int)(target[1] - '0');
	char originLetter = origin[0], targetLetter = target[0];
	string location = origin, temp = "";
	bool movesOnNumsPlus = false, movesOnNumsMinus = false, movesOnLettersPlus = false;

	/*checking if the move is valid (moving only on nums or only on letters)*/
	if (!(originLetter == targetLetter && originNum != targetNum || originLetter != targetLetter && originNum == targetNum))
	{
		return false; 
	}
	else
	{
		if (originNum < targetNum) /*checking if the nums change(get bigger)*/
		{
			temp = location;
			location = "";
			movesOnNumsPlus = true;
			location = location + temp[0] + (char)(temp[1] + 1); /*changing the location according to the move*/
		}
		else if(originNum > targetNum) /*checking if the nums change(get smaller)*/
		{
			temp = location;
			location = "";
			movesOnNumsMinus = true;
			location = location + temp[0] + (char)(temp[1] - 1); /*changing the location according to the move*/
		}
		else if (originLetter < targetLetter) /*checking if the letters change(get bigger)*/
		{
			temp = location;
			location = "";
			movesOnLettersPlus = true;
			location = location + (char)(temp[0] + 1) + temp[1]; /*changing the location according to the move*/
		}
		else /*checking if the letters change(get smaller)*/
		{
			temp = location;
			location = "";
			location = location + (char)(temp[0] - 1 ) + temp[1]; /*changing the location according to the move*/
		}

		/*running untill the location is equal to the target*/
		while (location.compare(target))
		{
			for (int i = 0; i < this->_pieces.size(); i++)
			{
				if (!(this->_pieces[i]->getLocation().compare(location))) /*there is a piece in the middle - the rook can't move to the target*/
				{
					return false;
				}
			}
			/*changing the location according to the style of moving*/
			if (movesOnNumsPlus)
			{
				temp = location;
				location = "";
				location = location + temp[0] + (char)(temp[1] + 1); /*changing the location according to the move*/
			}
			else if (movesOnNumsMinus)
			{
				temp = location;
				location = "";
				location = location + temp[0] + (char)(temp[1] - 1); /*changing the location according to the move*/
			}
			else if (movesOnLettersPlus)
			{
				temp = location;
				location = "";
				location = location + (char)(temp[0] + 1) + temp[1]; /*changing the location according to the move*/
			}
			else
			{
				temp = location;
				location = "";
				location = location + (char)(temp[0] - 1) + temp[1]; /*changing the location according to the move*/
			}
		}
	}

	return true;
}

/*checking if the king moves right*/
bool GameManager::checkIfKingCanMoveRight(std::string origin, std::string target)
{
	/*variables definition*/
	int originNum = (int)(origin[1] - '0'), targetNum = (int)(target[1] - '0');
	char originLetter = origin[0], targetLetter = target[0];

	/*checking all the possible moves of the king*/
	if (!(targetNum == originNum + 1 && originLetter == targetLetter || targetNum == originNum - 1 && originLetter == targetLetter
		|| targetNum == originNum && (originLetter + 1) == targetLetter || targetNum == originNum && (originLetter - 1) == targetLetter
		|| targetNum + 1 == originNum && (originLetter + 1) == targetLetter || targetNum - 1 == originNum && (originLetter - 1) == targetLetter
		|| targetNum + 1 == originNum && (originLetter - 1) == targetLetter || targetNum - 1 == originNum && (originLetter + 1) == targetLetter))
	{
		return false;
	}

	return true;

}

/*checking if the bishop moves right*/
bool GameManager::checkIfBishopCanMoveRight(std::string origin, std::string target)
{
	/*variables definition*/
	int originNum = (int)(origin[1] - '0'), targetNum = (int)(target[1] - '0');
	int moveNum = 0, moveLetter = 0;
	char originLetter = origin[0], targetLetter = target[0];
	string location = origin, temp = "";

	/*checking if the bishop really tries to move as it should move*/
	if (!(originNum != targetNum && originLetter != targetLetter))
	{
		return false;
	}
	else
	{
		/*checking how we need to move in order to check the location latter*/
		if (originNum < targetNum && originLetter < targetLetter)
		{
			moveNum = 1;
			moveLetter = 1;
		}
		else if (originNum > targetNum && originLetter > targetLetter)
		{
			moveNum = -1;
			moveLetter = -1;
		}
		else if (originNum > targetNum && originLetter < targetLetter)
		{
			moveNum = -1;
			moveLetter = 1;
		}
		else
		{
			moveNum = 1;
			moveLetter = -1;
		}

		temp = location;
		location = "";
		location = location + (char)(temp[0] + moveLetter) + (char)(temp[1] + moveNum); /*changing the location for the fisrt time*/

		/*running untill the location is equal to the target*/
		while (location.compare(target))
		{
			for (int i = 0; i < this->_pieces.size(); i++)
			{
				if (!(this->_pieces[i]->getLocation().compare(location))) /*there is a piece in the middle - the bishop can't move to the target*/
				{
					return false;
				}
			}

			temp = location;
			location = "";
			location = location + (char)(temp[0] + moveLetter) + (char)(temp[1] + moveNum); /*changing the location*/
		}

	}

	return true;
}


/*checking if the queen moves right*/
bool GameManager::checkIfQueenCanMoveRight(std::string origin, std::string target)
{
	/*variables definition*/
	int originNum = (int)(origin[1] - '0'), targetNum = (int)(target[1] - '0');
	char originLetter = origin[0], targetLetter = target[0];

	/*checking if the queen moves like a rook*/
	if (originLetter == targetLetter && originNum != targetNum || originLetter != targetLetter && originNum == targetNum)
	{
		return checkIfRookCanMoveRight(origin, target);
	}
	else /*checking if the queen moves like a bishop*/
	{
		return checkIfBishopCanMoveRight(origin, target); 
	}

}

/*checking if the knight moves right*/
bool GameManager::checkIfKnightCanMoveRight(std::string origin, std::string target)
{
	/*variables definition*/
	int originNum = (int)(origin[1] - '0'), targetNum = (int)(target[1] - '0');
	char originLetter = origin[0], targetLetter = target[0];

	/*checking all the possible moves of the knight*/
	if (!(targetNum == originNum + 1 && targetLetter == originLetter + 2 || targetNum == originNum - 1 && targetLetter == originLetter + 2
		|| targetNum == originNum + 2 && targetLetter == originLetter + 1 || targetNum == originNum + 2 && targetLetter == originLetter - 1
		|| targetNum == originNum + 1 && targetLetter == originLetter - 2 || targetNum == originNum - 1 && targetLetter == originLetter - 2
		|| targetNum == originNum - 2 && targetLetter == originLetter + 1 || targetNum == originNum - 2 && targetLetter == originLetter - 1))
	{
		return false;
	}
	
	return true;
}

/*checking if the pawn moves right*/
bool GameManager::checkIfPawnCanMoveRight(std::string origin, std::string target, char color)
{
	/*variables definition*/
	int originNum = (int)(origin[1] - '0'), targetNum = (int)(target[1] - '0');
	char originLetter = origin[0], targetLetter = target[0];
	string location = "", temp = "";

	location = location + originLetter + (char)(originNum + 1 + '0'); /*change the location for the fisrt time*/

	/*the white pawn tries to move two steps*/
	if (originNum + 2 == targetNum && originLetter == targetLetter && color == 'w')
	{
		if (getPawnInLocation(origin)->_ifPawnMovedAlready) /*the pawn moved already*/
		{
			return false;
		}
		else
		{
			location = "";
			location = location + originLetter + (char)(originNum + 1 + '0'); /*change the location for the fisrt time*/
			for (int i = 0; i < this->_pieces.size(); i++)
			{
				if (!(this->_pieces[i]->getLocation().compare(location))) /*checking if someone interrupt*/
				{
					return false;
				}
			}

			location = "";
			location = location + originLetter + (char)(originNum + 2 + '0'); /*change the location*/

			for (int i = 0; i < this->_pieces.size(); i++)
			{
				if (!(this->_pieces[i]->getLocation().compare(location))) /*checking if someone interrupt*/
				{
					return false;
				}
			}
		}
	}
	else if (originNum - 2 == targetNum && originLetter == targetLetter && color == 'b') /*the black pawn tries to move two steps*/
	{
		if (getPawnInLocation(origin)->_ifPawnMovedAlready) /*the pawn moved already*/
		{
			return false;
		}
		else
		{
			location = "";
			location = location + originLetter + (char)(originNum - 1 + '0'); /*change the location for the fisrt time*/
			for (int i = 0; i < this->_pieces.size(); i++)
			{
				if (!(this->_pieces[i]->getLocation().compare(location))) /*checking if someone interrupt*/
				{
					return false;
				}
			}

			location = "";
			location = location + originLetter + (char)(originNum - 2 + '0'); /*change the location*/
			for (int i = 0; i < this->_pieces.size(); i++)
			{
				if (!(this->_pieces[i]->getLocation().compare(location))) /*checking if someone interrupt*/
				{
					return false;
				}
			}
		}
	}
	else if(originNum + 1 == targetNum && originLetter + 1 == targetLetter && color == 'w') /*the white pawn tries to kill*/
	{
		location = "";
		location = location + targetLetter + (char)(targetNum + '0');

		if (getPieceInLocation(location) == nullptr || getPieceInLocation(location)->getColor() == this->_turn)
		{
			return false;
		}

	}
	else if (originNum + 1 == targetNum && originLetter - 1 == targetLetter && color == 'w') /*the white pawn tries to kill*/
	{
		location = "";
		location = location + (char)(originLetter - 1) + (char)(originNum + 1 + '0');

		if (getPieceInLocation(location) == nullptr || getPieceInLocation(location)->getColor() == this->_turn) 
		{
			return false;
		}
	}
	else if (originNum - 1 == targetNum && originLetter + 1 == targetLetter && color == 'b') /*the black pawn tries to kill*/
	{
		location = "";
		location = location + targetLetter + (char)(targetNum + '0');

		if (getPieceInLocation(location) == nullptr || getPieceInLocation(location)->getColor() == this->_turn)
		{
			return false;
		}

	}
	else if (originNum - 1 == targetNum && originLetter - 1 == targetLetter && color == 'b') /*the black pawn tries to kill*/
	{
		location = "";
		location = location + targetLetter + (char)(targetNum + '0');

		if (getPieceInLocation(location) == nullptr || getPieceInLocation(location)->getColor() == this->_turn)
		{
			return false;
		}
	}
	else if (originNum + 1 < targetNum && color == 'w' || (originNum - 1 > targetNum && color == 'b')) /*checking if it invalid move of the pawn*/
	{
		return false;
	}
	else if (color == 'w' && targetNum < originNum || color == 'b' && originNum < targetNum) /*checking if it invalid move of the pawn*/
	{
		return false;
	}
	else if (color == 'w' && getPieceInLocation(location) != nullptr && (originNum + 1) == targetNum) /*trying to go foward but there is a piece there - white piece tries to move*/
	{
		return false;
	}
	else if (color == 'b' && getPieceInLocation(location) != nullptr && (originNum - 3) == targetNum) /*trying to go foward but there is a piece there - black piece tries to move*/
	{
		return false;
	}
	else if (originLetter != targetLetter) /*the letters couldn't be different in this part of the check*/
	{
		return false;
	}

	getPawnInLocation(origin)->_ifPawnMovedAlready = true; /*the pawn moved*/

	return true;
}


/*The function checking if check will occure if we move the piece the user asked the program to*/
bool GameManager::ifKingIsUnderCheck(std::string origin, std::string target, std::string newLocation)
{
	/*variables definition*/
	short indexOfKing = 0;
	short movedPieceIndex = 0;
	Piece* removePiece = nullptr;
	string kingLocation = "";
	bool removed = false;

	/*Save the origin location because we want to move the piece out of the way.*/
	string tempLocation = origin;

	/*Moving the piece out of the way.*/
	for (int i = 0; i < this->_pieces.size(); i++)
	{
		if (!(this->_pieces[i]->getLocation().compare(origin))) 
		{
			/*moving the piece to other location*/
			movedPieceIndex = i;
			if (getPieceInLocation(newLocation) != nullptr)  /*there is a piece in the new location so we need to move it away*/
			{
				/*removing the piece away*/
				removePiece = getPieceInLocation(newLocation);
				removePiece->setterLocation("xx");
				removed = true;
			}
			this->_pieces[i]->setterLocation(newLocation); /*setting the new location*/
			break;
		}
	}

	/*Find the index of the needed king.*/
	for (int i = 0; i < this->_pieces.size(); i++)
	{
		if (this->_pieces[i]->getType() == 'K' && this->_turn == WHITE ||
			this->_pieces[i]->getType() == 'k' && this->_turn == BLACK)
		{
			indexOfKing = i;
			kingLocation = this->_pieces[i]->getLocation(); /*saving the king's location*/
			break;
		}
	}

	for (int i = 0; i < this->_pieces.size(); i++)
	{
		if (this->_pieces[i]->getColor() != this->_turn) /*checking if the pieces are enemys*/
		{
			if (ifPieceMoveRight(this->_pieces[i]->getType(), this->_pieces[i]->getLocation(), kingLocation)) /*checking if the enemy piece can go to the king's location*/
			{
				this->_pieces[movedPieceIndex]->setterLocation(tempLocation); /*returning the old location*/
				if (removed)
				{
					removePiece->setterLocation(newLocation); /*returning the old location*/
				}
				return true; /*there will be a check*/
			}
		}
	}

	return false; /*there won't be a check*/

}

/*getting pawn in specific location*/
Pawn* GameManager::getPawnInLocation(std::string location)
{
	for (int i = 0; i < this->_pieces.size(); i++) /*running over all the vector*/
	{
		if (!(this->_pieces[i]->getLocation().compare(location))) 
		{
			return (Pawn*)this->_pieces[i]; /*returning the pawn*/
		}
	}
}

