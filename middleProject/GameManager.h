#pragma once
#pragma once
#include "Piece.h"
#include <vector>
#include "Pawn.h"

class GameManager
{
public:
	/*class methods*/
	void initPiecesVector();
	std::string checkMove(std::string enteredMove);
	bool ifPieceMoveRight(char type, std::string origin, std::string target);
	bool checkIfRookCanMoveRight(std::string origin, std::string target);
	bool checkIfKingCanMoveRight(std::string origin, std::string target);
	bool checkIfBishopCanMoveRight(std::string origin, std::string target);
	bool checkIfQueenCanMoveRight(std::string origin, std::string target);
	bool checkIfKnightCanMoveRight(std::string origin, std::string target);
	bool checkIfPawnCanMoveRight(std::string origin, std::string target, char color);
	bool ifKingIsUnderCheck(std::string origin, std::string target, std::string newLocation);
	bool checkIfAfterThisMoveTheKingWillBeChecked(std::string origin, std::string target);
	Piece* getPieceInLocation(std::string location);
	void setTurn(char turn);
	Pawn* getPawnInLocation(std::string location);
	

private:
	/*members*/
	std::vector<Piece*> _pieces;
	char _turn = 'w';


};