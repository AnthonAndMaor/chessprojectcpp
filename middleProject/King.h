#pragma once
#include "Piece.h"


class King : public Piece
{
public:
	King(char type, char color, std::string location);
	~King();
	virtual void move(std::string location);
};